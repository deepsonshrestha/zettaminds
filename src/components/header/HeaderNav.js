import React from "react";
// import { Nav, Navbar } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import "./Header.css";

const HeaderNav = () => {
 

  return (
    <>
        <nav className="navbar home_nav_div navbar-expand-lg navbar-light ">
          <div className="container-fluid">
            <div className="logo_div">
              <NavLink className="navbar-brand logo_navbar" to="/">
                Zettaminds
              </NavLink>
            </div>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div
              className="collapse navbar-collapse "
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav me-auto mb-2 mb-lg-0 nav_item_div">
                <li className="nav-item">
                  <NavLink
                    className="nav-link nav_item_name active"
                    aria-current="page"
                    to=""
                  >
                    Home
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link nav_item_name" to="/about">
                    About US
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link nav_item_name" to="/projects">
                    Our Projects
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link nav_item_name" to="/contact">
                    Contact US
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="login_div">
              <NavLink className="nav-link login_btn" to="/login">
                Login
              </NavLink>
            </div>
          </div>
        </nav>
    </>
  );
};

export default HeaderNav;
