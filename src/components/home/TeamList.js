const teamList = [
  {
    id: "1",
    name: "Bibesh Paudel",
    designation: "Dot Net and Flutter Developer",
    image: "/images/teams/bibesh.jpg",
    link: "https://www.linkedin.com/in/bibesh-paudel-5a0207225/"
  },
  {
    id: "2",
    name: "Suabsh Pandey",
    designation: "MERN Stack Developer",
    image: "/images/teams/subash.jpg",
    link: "https://www.linkedin.com/in/subash-pandey-ab429a1a1/"
  },
  {
    id: "3",
    name: "Deepson Shrestha",
    designation: "Python Developer",
    image: "/images/teams/taku.jfif",
    link: "https://www.linkedin.com/in/deepsonshrestha/"
  },
  {
    id: "4",
    name: "Saugat Ghimire",
    designation: "UI/UX Designer",
    image: "/images/teams/solta.jpg",
    link: "https://www.facebook.com/saugat.ghimire.395"
  },
];

export default teamList;
