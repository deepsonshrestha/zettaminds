import React, { useEffect } from "react";
import Aos from "aos";
import "aos/dist/aos.css";
import teamList from "./TeamList";
import "./Home.css";

const Team = () => {
  useEffect(() => {
    Aos.init({
      duration: 3000,
    });
  }, []);

  return (
    <div>
      <div className="container">
        <div className="row">
          {teamList.map((devs) => {
            return (
              <>
                <div
                  data-aos="fade-up"
                  data-aos-duration="1500"
                  data-aos-easing="ease-in-sine"
                  className="col-lg-3 "
                  key={devs.id}
                >
                  <div className="card text-center card_div">
                    <div className="developer_img_div">
                    <a rel="noreferrer" href={devs.link} target="_blank">
                      <img
                        src={devs.image}
                        alt={devs.name}
                        className="developer_img"
                      />
                      </a>
                    </div>
                    <div className="card-body text-dark">
                      <h4 className="dev_name card-title">{devs.name}</h4>
                      <p className="designation card-text text-secondary">
                        {devs.designation}
                      </p>
                    </div>
                  </div>
                </div>
              </>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Team;
