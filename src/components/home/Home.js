import React, { useEffect } from "react";
import "./Home.css";
import Aos from "aos";
import "aos/dist/aos.css";
import Team from "./Team";

const Home = () => {
  useEffect(() => {
    Aos.init({
      duration: 2000,
    });
  }, []);

  return (
    <>
      <div className="bg_home">
        <div className="container">
          <div className="row">
            <div className="col-lg-8  p-0">
              <h1 data-aos="fade-right" className="zetta_title">
                Zettaminds
              </h1>
              <p className="zetta_para" data-aos="fade-up">
                A mind you always need.
              </p>
              <p className="zetta_para" data-aos="fade-up">
                Based in Kathmandu, Zettaminds is a fast-growing software
                startup company in the tech space. Our team consists of
                determined, energetic youths, each having a unique set of skills
                and discrete professional experience. We are a highly cohesive
                Software and AI development company, who focus on completion of
                tasks with great efficiency and we go above and beyond to
                satisfy our customers.
              </p>
            </div>
            <div className="col-lg-4 col-md-12 p-0 m-0">
              <div data-aos="fade-left" className="mind_img_div">
                <img
                  src="/images/home/mind2.png"
                  alt="Minds"
                  className="mind_img"
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Goals and Vision Section */}

      <section className="goals_vision_section">
        <div className="container">
          <div className="row">
            <div className="col-lg-4">
              <div data-aos="fade-right" className="goal_vision_img_div">
                <img
                  src="/images/goals/goals1.svg"
                  alt="goals"
                  className="goal_vision_img"
                />
              </div>
            </div>
            <div data-aos="fade-left" className="col-lg-8 col-md-12">
              <h1 className="goals_vision_title tc">Goals and Vision</h1>
              <p className="goal_para tc">
                We strive to develop cutting edge solutions for our clients
                through state-of-the-art technology, innovation, leadership and
                partnerships.
              </p>
            </div>
          </div>
        </div>
      </section>

      {/* Background Section */}
      <section className="background_section">
        <div className="bg_section ">
          <div className="bg_section_img_div">
            <img
              src="/images/home/bg1.jpg"
              alt="background"
              className="bg_section_img"
            />
            <div className="bg_section_content">
              <h1 data-aos="fade-up" className="ai_based">
                AI Based Zettaminds
              </h1>
              <p data-aos="fade-down" className="ai_based_para">
                We are an AI based Software Company aiming to provide high
                quality and superior products and services to our customers with
                state of the art features and technologies to leap through their
                dreams
              </p>
            </div>
          </div>
        </div>
      </section>

      {/* Team Section */}

      <section className="team_section">
        <div className="container-fluid bg_team">
          <div className="row">
            <div className="col-lg-12">
              <h1 data-aos="fade-up   " className="our_team_heading tc">
                Team Zettaminds
              </h1>
              <div className="team_main_div container">
                <Team />
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* Why you should join Zettaminds */}

      <section className="why_should_try_section">
        <div className="container-fluid bg_why_should">
          <div className="row">
            <div className="col-lg-8">
              <div className="why_you_should_join_para">
                <h1 className="why_you_should_title">
                  Why You should join Zettaminds
                </h1>

                <div className="why_you_should_join_para_div">
                  <p className="why_you_should_join_para">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Officia, aliquam! Eos aperiam ratione quas doloribus
                    cupiditate accusantium vitae sequi illo nam ut laborum
                    atque, explicabo ipsam possimus natus veniam corporis!
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="why_you_should_img_div">
                <img
                  src="/images/goals/goals.svg"
                  alt="you_should_img"
                  className="why_you_should_img"
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Home;
